function divinationChanged(option) {
    let descrContainer = document.getElementById('divination-description');
    let descrId = document.getElementById(`description-${option.value}`);
    descrContainer.innerHTML = descrId.innerText;
}