public class OneRuneDivination extends Divination {
    public override DivinationResult getResult(String divinationType) {
        DivinationResult result = new DivinationResult();
        result.runes = new List<DivinationResult.Rune>();
        
        Integer runeOrder = this.getRandomRune();
        String soql = 'SELECT Id, Name, Rune_Order__c, Rune_Description__c ' +
                        'FROM Rune__c ' + 
                        'WHERE Rune_Order__c = ' + runeOrder;
        Rune__c rune = Database.query(soql);

        Divination__c divination = getDivinationObject(divinationType);
        
        result.divinationDescription = divination.Divination_Description__c;
        result.divinationName = divination.Name;
        
        DivinationResult.Rune r = new DivinationResult.Rune();
        r.order = runeOrder;
        r.name = rune.Name;
        r.manticDescription = rune.Rune_Description__c;
        result.runes.add(r);
        
        result.resultUrl = '/apex/DivinationResult';
        
        return result;
    }
}