public class DivinationFactory {
    public static Divination getDivination(String divinationType) {
        Divination divination;
         
        switch on divinationType {
            when 'ONE_RUNE' {
                divination = new OneRuneDivination();
            }
            when 'THREE_RUNES' {
                divination = new ThreeRunesDivination();
            }
            when 'TWO_RUNES' {
                divination = new TwoRunesDivination();
            }
            when else {

            }
        }

        return divination;
    }
}