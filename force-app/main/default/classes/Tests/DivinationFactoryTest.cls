@isTest
public class DivinationFactoryTest {
    @isTest public static void testDivinationFactoryOneRune() {
        Divination__c div = 
            new Divination__c(Name='qq', Divination_Type__c='ONE_RUNE', order__c=1);
        insert div;
        
        DivinationFactory.getDivination('ONE_RUNE');
    }
    
    @isTest public static void testDivinationFactoryThreeRunes() {
        Divination__c div = 
            new Divination__c(Name='qq', Divination_Type__c='THREE_RUNES', order__c=1);
        insert div;
        
        DivinationFactory.getDivination('THREE_RUNES');
    }
}