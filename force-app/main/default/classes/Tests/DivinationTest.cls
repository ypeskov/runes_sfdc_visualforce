@isTest
public class DivinationTest {
    public static Integer runs = 0;

    @isTest public static void testOneRuneDivination() {
        List<Rune__c> runes = setupRunes();
        
        Divination__c div = 
            new Divination__c(Name='One Rune Divination', Divination_Type__c='ONE_RUNE', order__c=1);
        insert div;
        
        Divination divination = new OneRuneDivination();
        DivinationResult result = divination.getResult('ONE_RUNE');

        System.assertEquals('One Rune Divination', result.divinationName);
        System.assertEquals('/apex/DivinationResult', result.resultUrl);
        System.assertEquals(1, result.runes.size());
    }
    
    // @isTest public static void megaRunOfThreeRunes() {
    //     for(Integer i=0; i< 20; i++) {
    //         testThreeRunesDivination();
    //     }
        
    // }

    @isTest public static void testThreeRunesDivination() {
        final String divType = 'THREE_RUNES';

        if (runs < 1) {
            setupDB(divType);
            runs++;
        }
        
        Divination divination = new ThreeRunesDivination();
        DivinationResult result = divination.getResult('THREE_RUNES');

        System.assertEquals('/apex/DivinationResult', result.resultUrl);
        System.assertEquals(divType, result.divinationName);
        System.assertEquals(3, result.runes.size());
    }


    @isTest public static void testTwoRunesDivination() {
        final String divType = 'TWO_RUNES';
        if (runs < 1) {
            setupDB(divType);
            runs++;
        }
        
        Divination divination = new TwoRunesDivination();
        DivinationResult result = divination.getResult('TWO_RUNES');

        System.assertEquals('/apex/DivinationResult', result.resultUrl);
        System.assertEquals(divType, result.divinationName);
        System.assertEquals(2, result.runes.size());
    }

    private static void setupDB(String divinationType) {
        List<Rune__c> runes = setupRunes();
                
        Divination__c div = new Divination__c(Name=divinationType, 
                                        Divination_Type__c=divinationType,
                                        order__c=3);
        insert div;
    }

    public static Rune__c[] setupRunes() {
        List<Rune__c> runes = new List<Rune__c>();

        for(Integer i=0; i<24; i++) {
            runes.add(new Rune__c(Name='rune-'+i, Rune_Order__c=i));
        }
        insert runes;

        return runes;
    }


}