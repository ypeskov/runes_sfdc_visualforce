public class TwoRunesDivination extends Divination {
    protected final Integer NUMBER_OF_RUNES_TO_SELECT = 2;

	public override DivinationResult getResult(String divinationType) {
        DivinationResult result = new DivinationResult();
        result.runes = new List<DivinationResult.Rune>();

        Divination__c divination = getDivinationObject(divinationType);
        
        result.divinationDescription = divination.Divination_Description__c;
        result.divinationName = divination.Name;
        result.resultUrl = '/apex/DivinationResult';
        
        List<Rune__c> runes = getRunes(NUMBER_OF_RUNES_TO_SELECT);

        for(Rune__c r : runes) {
            DivinationResult.Rune rObject = new DivinationResult.Rune();
            rObject.name = r.name;
            rObject.order = r.Rune_Order__c.intValue();
            rObject.manticDescription = r.Rune_Description__c;
            result.runes.add(rObject);
        }

        return result;
    }

    public TwoRunesDivination() {

    }
}
