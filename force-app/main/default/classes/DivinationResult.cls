public class DivinationResult {
    public String divinationName {get;set;}
    
    public String divinationDescription {get;set;}
    
    public String resultUrl {get;set;}

    public List<Rune> runes {get;set;}

    public class Rune {
        public Integer order {get;set;}
        public String name {get;set;}
        public String manticDescription {get;set;}
    }
}