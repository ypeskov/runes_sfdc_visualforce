public class DivinationController {
    public String divinationType {get;set;}
    public List<Divination__c> divinations {get;set;}
    public DivinationResult result {get;set;}
    
    public DivinationController() {
        divinations = 
            [SELECT Id, Name, Divination_Description__c, Divination_Type__c 
            FROM Divination__c
            ORDER BY order__c];
    }

    public List<SelectOption> getOptions() {
        List<SelectOption> options = new List<SelectOption>();
        
        for(Divination__c d : divinations) {
            options.add(new SelectOption(d.Divination_Type__c, d.Name));
        }
        
        return options;
	}
    
    public PageReference selectDivination() {
        Divination divination = DivinationFactory.getDivination(this.divinationType);
        
        result = divination.getResult(divinationType);
        
        PageReference resultPage = new PageReference(result.resultUrl);
        resultPage.setRedirect(false);

        return resultPage;
	}
    
    public PageReference start() {
        PageReference page = new PageReference('/apex/DivinationStart');
        page.setRedirect(false);
        
        return page;
    }
}