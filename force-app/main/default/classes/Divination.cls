public abstract class Divination {
    protected final Integer NUMBER_OF_RUNES = 24;
    protected List<Integer> runeOrders;
    
    abstract public DivinationResult getResult(String divinationType);

    public Divination() {
        runeOrders = new List<Integer>();

        for(Integer i=1; i<=NUMBER_OF_RUNES; i++) {
            runeOrders.add(i);
        }
    }
    
    protected Integer getRandomRune() {
        return (Integer) Math.ceil(Math.random() * NUMBER_OF_RUNES);
    }
    
    protected Divination__c getDivinationObject(String DivinationType) {
        String soql = 'SELECT Id, Name, Divination_Description__c ' +
                'FROM Divination__c ' +
                'WHERE Divination_Type__c = \'' + divinationType + '\'';
        Divination__c divination = Database.query(soql);

        return divination;
    }

    protected List<Rune__c> getRunes(Integer numberRunes) {
        List<Rune__c> runes = new List<Rune__c>();
        List<Integer> orders = new List<Integer>();

        while(orders.size() < numberRunes) {
            Integer index = (Math.random() * (runeOrders.size() - 1)).intValue();
            orders.add(runeOrders[index]);
            runeOrders.remove(index);
        }
        runes = [SELECT Id, Name, Rune_Description__c, Rune_Order__c From Rune__c 
                WHERE Rune_Order__c IN :orders];
        
        return runes;
    }
}