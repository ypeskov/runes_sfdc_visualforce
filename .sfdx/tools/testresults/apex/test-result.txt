=== Test Reports
FORMAT  FILE
──────  ──────────────────────────────────────────────────────────────────────────────────────────────────────────
txt     c:\Users\user1\Projects\divination_sfdc\.sfdx\tools\testresults\apex\test-result.txt
txt     c:\Users\user1\Projects\divination_sfdc\.sfdx\tools\testresults\apex\test-run-id.txt
junit   c:\Users\user1\Projects\divination_sfdc\.sfdx\tools\testresults\apex\test-result-7071X00000ARcy4-junit.xml
json    c:\Users\user1\Projects\divination_sfdc\.sfdx\tools\testresults\apex\test-result-7071X00000ARcy4.json

=== Test Results
TEST NAME                                              OUTCOME  MESSAGE  RUNTIME (MS)
─────────────────────────────────────────────────────  ───────  ───────  ────────────
DivinationFactoryTest.testDivinationFactoryOneRune     Pass              126
DivinationFactoryTest.testDivinationFactoryThreeRunes  Pass              52
DivinationTest.testOneRuneDivination                   Pass              468
DivinationTest.testThreeRunesDivination                Pass              532
DivinationTest.testTwoRunesDivination                  Pass              148
TestDivinationController.testStart                     Pass              41

=== Test Summary
NAME                 VALUE
───────────────────  ──────────────────────────────────────────────────────────
Outcome              Passed
Tests Ran            6
Passing              6
Failing              0
Skipped              0
Pass Rate            100%
Fail Rate            0%
Test Start Time      Jun 6, 2019 10:42 AM
Test Execution Time  1367 ms
Test Total Time      1367 ms
Command Time         5102 ms
Hostname             https://connect-speed-3342-dev-ed.cs101.my.salesforce.com
Org Id               00D1X0000000RPbUAM
Username             test-qaryur0qnp95@example.com
Test Run Id          7071X00000ARcy4
User Id              0051X000001FFbqQAG

