declare module "@salesforce/resourceUrl/divination" {
    var divination: string;
    export default divination;
}
declare module "@salesforce/resourceUrl/jQuery" {
    var jQuery: string;
    export default jQuery;
}
declare module "@salesforce/resourceUrl/styles" {
    var styles: string;
    export default styles;
}
